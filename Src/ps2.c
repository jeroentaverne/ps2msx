#include "hal.h"
#include "ps2.h"

#define PS2_KEYMAP_SIZE 132 // PS2 matrix size with MSX bytes

static volatile uint32_t prev_ms=0;
static volatile bool cmd_in_progress = false;
static volatile uint8_t cmd_value = 0;
static volatile int cmd_count = 0;
static volatile bool cmd_parity = false;
static volatile bool busy = false;

typedef struct
{
	uint8_t normal[PS2_KEYMAP_SIZE];
	uint8_t extended[PS2_KEYMAP_SIZE];
} PS2Keymap_t;

static PS2Keymap_t keymap =
{
	//Normal
	{
		0xB7,
		0xB7 /* F9 */,
		0xB7,
		0x71 /* F5 */,
		0x67 /* F3 */,
		0x65 /* F1 */,
		0x66 /* F2 */,
		0xB7 /* F12 */,
		0xB7,
		0xB7 /* F10 */,
		0xB7 /* F8 */,
		0xB7 /* F6 */,
		0x70 /* F4 */,
		0x73 /* TAB */,
		0xB7 /* ` */,
		0xB7,
		0xB7,
		0x64 /* L ALT */,
		0x60 /* L SHFT */,
		0xB7,
		0x61 /* L CTRL */,
		0x46 /* Q */,
		0x01 /* 1 */,
		0xB7,
		0xB7,
		0xB7,
		0x57 /* Z */,
		0x50 /* S */,
		0x26 /* A */,
		0x54 /* W */,
		0x02 /* 2 */,
		0xB7,
		0xB7,
		0x30 /* C */,
		0x55 /* X */,
		0x31 /* D */,
		0x32 /* E */,
		0x04 /* 4 */,
		0x03 /* 3 */,
		0xB7,
		0xB7,
		0x80 /* SPACE */,
		0x53 /* V */,
		0x33 /* F */,
		0x51 /* T */,
		0x47 /* R */,
		0x05 /* 5 */,
		0xB7,
		0xB7,
		0x43 /* N */,
		0x27 /* B */,
		0x35 /* H */,
		0x34 /* G */,
		0x56 /* Y */,
		0x06 /* 6 */,
		0xB7,
		0xB7,
		0xB7,
		0x42 /* M */,
		0x37 /* J */,
		0x52 /* U */,
		0x07 /* 7 */,
		0x10 /* 8 */,
		0xB7,
		0xB7,
		0x22 /*, */,
		0x40 /* K */,
		0x36 /* I */,
		0x44 /* O */,
		0x00 /* 0 */,
		0x11 /* 9 */,
		0xB7,
		0xB7,
		0x23 /* . */,
		0x25 /* ; */,
		0x41 /* L */,
		0x17 /* � */,
		0x45 /* P */,
		0x12 /* - */,
		0xB7,
		0xB7,
		0x24, /* / */
		0x20 /* ~ */,
		0xB7,
		0x15 /* AGUDO */,
		0x13 /* = */,
		0xB7,
		0xB7,
		0x63 /* CAPS */,
		0x60 /* R SHFT */,
		0x77 /* ENTER */,
		0x16 /* [ */,
		0xB7,
		0x21 /* ] */,
		0xB7,
		0xB7,
		0xB7,
		0x14,
		0xB7,
		0xB7,
		0xB7,
		0xB7,
		0x75 /* BKSP */,
		0xB7,
		0xB7,
		0x94 /* KP 1 */,
		0xB7,
		0x97 /* KP 4 */,
		0xa2 /* KP 7 */,
		0xB7,
		0xB7,
		0xB7,
		0x93 /* KP 0 */,
		0xa7 /* KP . */,
		0x95 /* KP 2 */,
		0xa0 /* KP 5 */,
		0xa1 /* KP 6 */,
		0xa3 /* KP 8 */,
		0x72 /* ESC */,
		0xB7 /* NUM */,
		0xB7 /* F11 */,
		0x91 /* KP + */,
		0x96 /* KP 3 */,
		0xa5 /* KP - */,
		0x90 /* KP * */,
		0xa4 /* KP 9 */,
		0x74 /* SCROLL */,
		0xB7,
		0xB7,
		0xB7,
		0xB7,
		0xB7 /* F7 */
	},
	//Extended
	{
		0xB7,
		0xB7,
		0xB7,
		0xB7,
		0xB7,
		0xB7,
		0xB7,
		0xB7,
		0xB7,
		0xB7,
		0xB7,
		0xB7,
		0xB7,
		0xB7,
		0xB7,
		0xB7,
		0xB7 /* WWW Search */,
		0x64 /* R ALT */,
		0xB7 /* PRNT SCRN */,
		0xB7,
		0x61 /* R CTRL */,
		0xB7 /* Previous Track */,
		0xB7,
		0xB7,
		0xB7,
		0xB7,
		0xB7,
		0xB7,
		0xB7,
		0xB7,
		0xB7,
		0x62 /* L WIN */,
		0xB7,
		0xB7,
		0xB7,
		0xB7,
		0xB7,
		0xB7 /* MESSENGER */,
		0xB7,
		0x62 /* R WIN */,
		0xB7,
		0xB7,
		0xB7,
		0xB7 /* CALC */,
		0xB7,
		0xB7,
		0xB7,
		0xB7 /* CONTEXTO */,
		0xB7,
		0xB7,
		0xB7,
		0xB7,
		0xB7 /* PLAY/PAUSE */,
		0xB7,
		0xB7,
		0xB7,
		0xB7,
		0xB7,
		0xB7 /* WWW HOME */,
		0xB7 /* STOP */,
		0xB7 /* LOG OFF */,
		0xB7,
		0xB7,
		0xB7 /* DORMIR */,
		0xB7 /* My Computer */,
		0xB7,
		0xB7,
		0xB7,
		0xB7,
		0xB7,
		0xB7,
		0xB7,
		0xB7 /* EMAIL */,
		0xB7,
		0x92 /* KP / */,
		0xB7,
		0xB7,
		0xB7 /* Next Track */,
		0xB7,
		0xB7,
		0xB7 /* Media Select */,
		0xB7,
		0xB7,
		0xB7,
		0xB7,
		0xB7,
		0xB7,
		0xB7,
		0xB7,
		0xB7,
		0x77 /* KP EN */,
		0xB7,
		0xB7,
		0xB7,
		0xB7 /* Wake */,
		0xB7,
		0xB7,
		0xB7,
		0xB7,
		0xB7,
		0xB7,
		0xB7,
		0xB7,
		0xB7,
		0xB7,
		0xB7 /* END */,
		0xB7,
		0x84 /* L ARROW */,
		0x81 /* HOME */,
		0xB7,
		0xB7,
		0xB7,
		0x82 /* INSERT */,
		0x83 /* DELETE */,
		0x86 /* D ARROW */,
		0xB7,
		0x87 /* R ARROW */,
		0x85 /* U ARROW */,
		0xB7,
		0xB7,
		0xB7,
		0xB7,
		0x76 /* PG DN */,
		0xB7,
		0xB7,
		0x74 /* PG UP */,
		0xB7,
		0xB7,
		0xB7,
		0xB7,
		0xB7,
		0xB7
	}
};

#define PS2_TAB				9
#define PS2_ENTER			13
#define PS2_BACKSPACE			127
#define PS2_ESC				27
#define PS2_INSERT			0
#define PS2_DELETE			127
#define PS2_HOME			0
#define PS2_END				0
#define PS2_PAGEUP			25
#define PS2_PAGEDOWN			26
#define PS2_UPARROW			11
#define PS2_LEFTARROW			8
#define PS2_DOWNARROW			10
#define PS2_RIGHTARROW			21
#define PS2_F1				0
#define PS2_F2				0
#define PS2_F3				0
#define PS2_F4				0
#define PS2_F5				0
#define PS2_F6				0
#define PS2_F7				0
#define PS2_F8				0
#define PS2_F9				0
#define PS2_F10				0
#define PS2_F11				0
#define PS2_F12				0
#define PS2_SCROLL			0
#define PS2_EURO_SIGN			0

const uint8_t ps2_ascii[] =
{
	0,
	PS2_F9,
	0,
	PS2_F5,
	PS2_F3,
	PS2_F1,
	PS2_F2,
	PS2_F12,
	0,
	PS2_F10,
	PS2_F8,
	PS2_F6,
	PS2_F4,
	PS2_TAB,
	'`',
	0,
	0,
	0 /*Lalt*/,
	0 /*Lshift*/,
	0,
	0 /*Lctrl*/,
	'q',
	'1',
	0,
	0,
	0,
	'z',
	's',
	'a',
	'w',
	'2',
	0,
	0,
	'c',
	'x',
	'd',
	'e',
	'4',
	'3',
	0,
	0,
	' ',
	'v',
	'f',
	't',
	'r',
	'5',
	0,
	0,
	'n',
	'b',
	'h',
	'g',
	'y',
	'6',
	0,
	0,
	0,
	'm',
	'j',
	'u',
	'7',
	'8',
	0,
	0,
	',',
	'k',
	'i',
	'o',
	'0',
	'9',
	0,
	0,
	'.',
	'/',
	'l',
	';',
	'p',
	'-',
	0,
	0,
	0,
	'\'',
	0,
	'[',
	'=',
	0,
	0,
	0 /*CapsLock*/,
	0 /*Rshift*/,
	PS2_ENTER /*Enter*/,
	']',
	0,
	'\\',
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	PS2_BACKSPACE,
	0,
	0,
	'1',
	0,
	'4',
	'7',
	0,
	0,
	0,
	'0',
	'.',
	'2',
	'5',
	'6',
	'8',
	PS2_ESC,
	0 /*NumLock*/,
	PS2_F11,
	'+',
	'3',
	'-',
	'*',
	'9',
	PS2_SCROLL,
	0,
	0,
	0,
	0,
	PS2_F7
};

extern uint8_t matrix[];

void ps2_init(void)
{
	GPIO_INIT(PS2_CLK_IN, GPIO_MODE_IT_FALLING);
	GPIO_INIT(PS2_CLK_OUT, GPIO_MODE_OUTPUT_OD);
	GPIO_INIT(PS2_DATA, GPIO_MODE_OUTPUT_OD);
	GPIO_INIT(LED, GPIO_MODE_OUTPUT_OD);
	GPIO_INIT(CAPS, GPIO_MODE_INPUT);
	HAL_Delay(2000);
	//ps2_send(0xff);
	//HAL_Delay(2000);
	ps2_send(0xed);
	HAL_Delay(100);
}

void ps2_clock_update_receive(void)
{
	static uint32_t bit_index=0;
	static bool start_bit;
	static uint32_t data_word=0;
	static bool stop_bit = false;
	static bool parity_bit = false;
	static bool ps2Keyboard_release = false;
	static bool ps2Keyboard_extend = false;
	static bool cmd_ack = false;
	bool data_state;

	if (GPIO_GET(PS2_DATA))
		data_state = true;
	else
		data_state = false;
	uint32_t volatile now_ms = HAL_GetTick();

	if (__HAL_GPIO_EXTI_GET_IT(PS2_CLK_IN_Pin))
	{
		__HAL_GPIO_EXTI_CLEAR_IT(PS2_CLK_IN_Pin);

		if (cmd_in_progress)
		{
			cmd_count++;          // cmd_count keeps track of the shifting
			switch (cmd_count)
			{
			case 1:
				// start bit
				break;
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
			case 7:
			case 8:
			case 9:
				// data bits to shift
				if (cmd_value & 1)
					GPIO_SET(PS2_DATA);
				else
					GPIO_RESET(PS2_DATA);
				cmd_value = cmd_value>>1;
				break;
			case 10:  // parity bit
				if (cmd_parity)
					GPIO_SET(PS2_DATA);
				else
					GPIO_RESET(PS2_DATA);
				break;
			case 11:  // stop bit
				// release the data pin, so stop bit actually relies on pull-up
				// but this ensures the data pin is ready to be driven by the kbd for
				// for the next bit.
				GPIO_SET(PS2_DATA);
				break;
			case 12: // ack bit - driven by the kbd, so we read its value
				if (GPIO_GET(PS2_DATA))
					cmd_ack = true;
				else
					cmd_ack = false;
				cmd_in_progress = false;  // done shifting out
			}
			return; // don't fall through to the receive section of the ISR
		}

		if(now_ms-prev_ms>250)
		{
			bit_index=0;
			busy = false;
		}
		prev_ms=now_ms;

		if(bit_index==0)
		{
			data_word=0;
			start_bit=data_state;
			if(start_bit!=0)
			{
				return;
			}
			busy = true;
		}
		else if(bit_index>0 && bit_index<=8) // collect bits
		{
			data_word|=(data_state<<(bit_index-1));
		}
		else if(bit_index==9)
		{
			parity_bit=data_state;
		}
		else if(bit_index==10)
		{
			stop_bit=data_state;
		}
		bit_index++;
		if(bit_index==11) // 8 + start + stop + parity
		{
			busy = false;
			bool parity_ok=__builtin_parity((data_word<<1)|parity_bit);
			if(start_bit==0 && stop_bit==1 && parity_ok)
			{
				switch (data_word)
				{
				case 0xf0:   // key release char
				{
					ps2Keyboard_release = true;
					break;
				}
				case 0xe0:   // extended char set
				{
					ps2Keyboard_extend = true;
					break;
				}
				default:   // a real key
				{
					if (data_word < PS2_KEYMAP_SIZE)
					{
						uint8_t linecolumn;
						if (!ps2Keyboard_extend)
						{
							linecolumn=keymap.normal[data_word];
						}
						else
						{
							linecolumn=keymap.extended[data_word];
							ps2Keyboard_extend=false;
						}
						if (linecolumn < 0xb7)
						{
							int line = linecolumn >> 4;
							int column = linecolumn & 15;
							if (ps2Keyboard_release)
							{
								matrix[1 << line] &= ~(1 << column);
								ps2Keyboard_release = false;
								GPIO_SET(LED);
							}
							else
							{
								matrix[1 << line] |= 1 << column;
								GPIO_RESET(LED);
							}
						}
						else
						{
							ps2Keyboard_release = false;
						}
					}
				}
				}
			}
			bit_index=0;
		}
	}
}

void ps2_send(uint8_t data)
{
	while  (cmd_in_progress || busy);
	cmd_in_progress = true;
	cmd_count = 0;
	cmd_value = data;
	cmd_parity = __builtin_parity(cmd_value);
	// Generate clock pulse for startbit
	GPIO_RESET(PS2_CLK_OUT);
	HAL_Delay(2);
	GPIO_RESET(PS2_DATA);
	GPIO_SET(PS2_CLK_OUT);
}

void ps2_task(void)
{
	return;
	static uint32_t oldcaps = 0xffffffff;
	uint32_t newcaps = GPIO_GET(CAPS);
	if (oldcaps != newcaps)
	{
		oldcaps = newcaps;
		if (newcaps)
			ps2_send(2);
		else
			ps2_send(6);
	}
}
