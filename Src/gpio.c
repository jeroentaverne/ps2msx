#include "gpio.h"

void gpio_init(GPIO_TypeDef *GPIOx, uint32_t Pin, uint32_t Mode)
{
	GPIO_InitTypeDef GPIO_InitStruct;

	if (Mode == GPIO_MODE_OUTPUT_OD)
		HAL_GPIO_WritePin(GPIOx, Pin, GPIO_PIN_SET);
	else
		HAL_GPIO_WritePin(GPIOx, Pin, GPIO_PIN_RESET);

	GPIO_InitStruct.Pin = Pin;
	GPIO_InitStruct.Mode = Mode;
	if (Mode == GPIO_MODE_OUTPUT_PP || Mode == GPIO_MODE_OUTPUT_OD || Mode == GPIO_MODE_ANALOG)
		GPIO_InitStruct.Pull = GPIO_NOPULL;
	else
		GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOx, &GPIO_InitStruct);

	if (Mode == GPIO_MODE_OUTPUT_OD || GPIO_InitStruct.Pull == GPIO_PULLUP)
		HAL_GPIO_WritePin(GPIOx, Pin, GPIO_PIN_SET);
	else
		HAL_GPIO_WritePin(GPIOx, Pin, GPIO_PIN_RESET);
}

void MX_GPIO_Init2(void)
{
	// Enable all clocks
	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_GPIOB_CLK_ENABLE();
	__HAL_RCC_GPIOC_CLK_ENABLE();
	__HAL_RCC_GPIOD_CLK_ENABLE();
	__HAL_RCC_GPIOE_CLK_ENABLE();

	// Just enable all external interrupts, they are masked anyway by default
	HAL_NVIC_SetPriority(EXTI0_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI0_IRQn);
	HAL_NVIC_SetPriority(EXTI1_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI1_IRQn);
	HAL_NVIC_SetPriority(EXTI2_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI2_IRQn);
	HAL_NVIC_SetPriority(EXTI3_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI3_IRQn);
	HAL_NVIC_SetPriority(EXTI4_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI4_IRQn);
	HAL_NVIC_SetPriority(EXTI9_5_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);
	HAL_NVIC_SetPriority(EXTI15_10_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);
}
