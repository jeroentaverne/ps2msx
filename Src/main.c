#include "main.h"
#include "gpio.h"

#define MATRIX_MASK 0x7ff

// MSX keyboard matrix
uint8_t matrix[MATRIX_MASK+1];

void SystemClock_Config(void);
static void MX_GPIO_Init(void);

int main(void)
{
	HAL_Init();

	SystemClock_Config();

	// Clear matrix, no keys pressed
	memset(matrix, 0, sizeof(matrix));

	MX_GPIO_Init();
	ps2_init();

	while (1)
	{
		// Handle the caps lock LED, rest is done by interrupt routines
		ps2_task();
	}
}

void SystemClock_Config(void)
{
	RCC_OscInitTypeDef RCC_OscInitStruct = {0};
	RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

	/** Initializes the RCC Oscillators according to the specified parameters
	* in the RCC_OscInitTypeDef structure.
	*/
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
	RCC_OscInitStruct.HSIState = RCC_HSI_ON;
	RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI_DIV2;
	RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL16;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	{
		Error_Handler();
	}
	/** Initializes the CPU, AHB and APB buses clocks
	*/
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
	                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
	{
		Error_Handler();
	}
}

static void MX_GPIO_Init(void)
{
	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_GPIOB_CLK_ENABLE();
	__HAL_RCC_GPIOC_CLK_ENABLE();
	__HAL_RCC_GPIOD_CLK_ENABLE();

	GPIO_INIT(TOGGLE, GPIO_MODE_OUTPUT_OD);

#if 0
	GPIO_INIT(X0, GPIO_MODE_OUTPUT_PP);
	GPIO_INIT(X1, GPIO_MODE_OUTPUT_PP);
	GPIO_INIT(X2, GPIO_MODE_OUTPUT_PP);
	GPIO_INIT(X3, GPIO_MODE_OUTPUT_PP);
	GPIO_INIT(X4, GPIO_MODE_OUTPUT_PP);
	GPIO_INIT(X5, GPIO_MODE_OUTPUT_PP);
	GPIO_INIT(X6, GPIO_MODE_OUTPUT_PP);
	GPIO_INIT(X7, GPIO_MODE_OUTPUT_PP);
#else
	GPIO_INIT(X0, GPIO_MODE_OUTPUT_OD);
	GPIO_INIT(X1, GPIO_MODE_OUTPUT_OD);
	GPIO_INIT(X2, GPIO_MODE_OUTPUT_OD);
	GPIO_INIT(X3, GPIO_MODE_OUTPUT_OD);
	GPIO_INIT(X4, GPIO_MODE_OUTPUT_OD);
	GPIO_INIT(X5, GPIO_MODE_OUTPUT_OD);
	GPIO_INIT(X6, GPIO_MODE_OUTPUT_OD);
	GPIO_INIT(X7, GPIO_MODE_OUTPUT_OD);
#endif

	GPIO_SET(X0);
	GPIO_SET(X1);
	GPIO_SET(X2);
	GPIO_SET(X3);
	GPIO_SET(X4);
	GPIO_SET(X5);
	GPIO_SET(X6);
	GPIO_SET(X7);

#if 0
	GPIO_INIT(Y0, GPIO_MODE_INPUT);
	GPIO_INIT(Y1, GPIO_MODE_INPUT);
	GPIO_INIT(Y2, GPIO_MODE_INPUT);
	GPIO_INIT(Y3, GPIO_MODE_INPUT);
	GPIO_INIT(Y4, GPIO_MODE_INPUT);
	GPIO_INIT(Y5, GPIO_MODE_INPUT);
	GPIO_INIT(Y6, GPIO_MODE_INPUT);
	GPIO_INIT(Y7, GPIO_MODE_INPUT);
	GPIO_INIT(Y8, GPIO_MODE_INPUT);
	GPIO_INIT(Y9, GPIO_MODE_INPUT);
	GPIO_INIT(Y10, GPIO_MODE_INPUT);
#else
	GPIO_INIT(Y0, GPIO_MODE_IT_FALLING);
	GPIO_INIT(Y1, GPIO_MODE_IT_FALLING);
	GPIO_INIT(Y2, GPIO_MODE_IT_FALLING);
	GPIO_INIT(Y3, GPIO_MODE_IT_FALLING);
	GPIO_INIT(Y4, GPIO_MODE_IT_FALLING);
	GPIO_INIT(Y5, GPIO_MODE_IT_FALLING);
	GPIO_INIT(Y6, GPIO_MODE_IT_FALLING);
	GPIO_INIT(Y7, GPIO_MODE_IT_FALLING);
	GPIO_INIT(Y8, GPIO_MODE_IT_FALLING);
	GPIO_INIT(Y9, GPIO_MODE_IT_FALLING);
	GPIO_INIT(Y10, GPIO_MODE_IT_FALLING);
#endif

	HAL_NVIC_SetPriority(EXTI0_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI0_IRQn);
	HAL_NVIC_SetPriority(EXTI1_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI1_IRQn);
	HAL_NVIC_SetPriority(EXTI2_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI2_IRQn);
	HAL_NVIC_SetPriority(EXTI3_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI3_IRQn);
	HAL_NVIC_SetPriority(EXTI4_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI4_IRQn);
	HAL_NVIC_SetPriority(EXTI9_5_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);
	HAL_NVIC_SetPriority(EXTI15_10_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);
}

void ychange(void)
{
	if (__HAL_GPIO_EXTI_GET_IT(MATRIX_MASK))
	{
		// Set all X lines floating
		GPIOB->BSRR = 0xff00;
		__HAL_GPIO_EXTI_CLEAR_IT(MATRIX_MASK);

		// Read active low Y lines and invert them
		int index = (GPIOA->IDR & MATRIX_MASK) ^ MATRIX_MASK;
		// Set X lines low from matrix. Matrix is filled by ps2.c
		GPIOB->BRR = matrix[index] << 8;
	}
}

void EXTI0_IRQHandler(void)
{
	ychange();
}

void EXTI1_IRQHandler(void)
{
	ychange();
}

void EXTI2_IRQHandler(void)
{
	ychange();
}

void EXTI3_IRQHandler(void)
{
	ychange();
}

void EXTI4_IRQHandler(void)
{
	ychange();
}

void EXTI9_5_IRQHandler(void)
{
	ychange();
}

void EXTI15_10_IRQHandler(void)
{
	ychange();
	ps2_clock_update_receive();
}

void Error_Handler(void)
{
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
	/* USER CODE BEGIN 6 */
	/* User can add his own implementation to report the file name and line number,
	   tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
	/* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
