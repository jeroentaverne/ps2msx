#pragma once

#include "hal.h"

#define Y0_Pin GPIO_PIN_0
#define Y0_Port GPIOA
#define Y1_Pin GPIO_PIN_1
#define Y1_Port GPIOA
#define Y2_Pin GPIO_PIN_2
#define Y2_Port GPIOA
#define Y3_Pin GPIO_PIN_3
#define Y3_Port GPIOA
#define Y4_Pin GPIO_PIN_4
#define Y4_Port GPIOA
#define Y5_Pin GPIO_PIN_5
#define Y5_Port GPIOA
#define Y6_Pin GPIO_PIN_6
#define Y6_Port GPIOA
#define Y7_Pin GPIO_PIN_7
#define Y7_Port GPIOA
#define Y8_Pin GPIO_PIN_8
#define Y8_Port GPIOA
#define Y9_Pin GPIO_PIN_9
#define Y9_Port GPIOA
#define Y10_Pin GPIO_PIN_10
#define Y10_Port GPIOA

#define X0_Pin GPIO_PIN_8
#define X0_Port GPIOB
#define X1_Pin GPIO_PIN_9
#define X1_Port GPIOB
#define X2_Pin GPIO_PIN_10
#define X2_Port GPIOB
#define X3_Pin GPIO_PIN_11
#define X3_Port GPIOB
#define X4_Pin GPIO_PIN_12
#define X4_Port GPIOB
#define X5_Pin GPIO_PIN_13
#define X5_Port GPIOB
#define X6_Pin GPIO_PIN_14
#define X6_Port GPIOB
#define X7_Pin GPIO_PIN_15
#define X7_Port GPIOB

#define PS2_CLK_IN_Pin GPIO_PIN_15
#define PS2_CLK_IN_Port GPIOA
#define PS2_CLK_OUT_Pin GPIO_PIN_3
#define PS2_CLK_OUT_Port GPIOB
#define PS2_DATA_Pin GPIO_PIN_4
#define PS2_DATA_Port GPIOB

#define CAPS_Pin GPIO_PIN_5
#define CAPS_Port GPIOB

#define LED_Pin GPIO_PIN_13
#define LED_Port GPIOC

#define TOGGLE_Pin GPIO_PIN_14
#define TOGGLE_Port GPIOC

void gpio_init(GPIO_TypeDef *GPIOx, uint32_t Pin, uint32_t Mode);
void MX_GPIO_Init2(void);
