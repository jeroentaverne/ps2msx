#pragma once

void ps2_init(void);
void ps2_clock_update_receive(void);
void ps2_task(void);
void ps2_send(uint8_t data);
