#pragma once

#define CORTEX_M3

#ifdef CORTEX_M0
#include "stm32f0xx_hal.h"
#endif

#ifdef CORTEX_M3
#include "stm32f1xx_hal.h"
#endif

#ifdef CORTEX_M4
#include "stm32f3xx_hal.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#include <stdbool.h>
#include "gpio.h"

#define GPIO_GET_OUTPUT(PIN) (PIN ## _Port->ODR & (PIN ## _Pin))
#define GPIO_SET(PIN) PIN ## _Port->BSRR = (PIN ## _Pin)
#define GPIO_RESET(PIN) PIN ## _Port->BSRR = (PIN ## _Pin) << 16
#define GPIO_GET(PIN) (PIN ## _Port->IDR & (PIN ## _Pin))
#define GPIO_INIT(PIN, MODE) do {GPIO_RESET(PIN);gpio_init(PIN ## _Port, PIN ## _Pin, MODE);} while (0)

#define ID_FLASH_ADDRESS   0x1FFF7A22
#define FLASH_SIZE (*(uint16_t *)(ID_FLASH_ADDRESS))
